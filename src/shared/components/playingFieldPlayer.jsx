import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import PlayingFieldRowPlayer from './playingFieldRowPlayer';

const PlayingFieldPlayer = (props) => {
    return (
        <Fragment>
            <div className="col s6 gameCard">
                <div className="card">
                    <div className="card-content">
                        <div className="row">
                            <h7 className="col s3 offset-s5">{props.playerName}</h7>
                        </div>
                        <div className="row">
                            <div className="col s1 offset-s2">A</div>
                            <div className="col s1">B</div>
                            <div className="col s1">C</div>
                            <div className="col s1">D</div>
                            <div className="col s1">E</div>
                            <div className="col s1">F</div>
                            <div className="col s1">G</div>
                            <div className="col s1">H</div>
                            <div className="col s1">I</div>
                            <div className="col s1">J</div>
                        </div>
                        <div className="row">
                            <Fragment>
                                <PlayingFieldRowPlayer index={1} ship={props.ships[0]} />
                                <PlayingFieldRowPlayer index={2} ship={props.ships[1]} />
                                <PlayingFieldRowPlayer index={3} ship={props.ships[2]} />
                                <PlayingFieldRowPlayer index={4} ship={props.ships[3]} />
                                <PlayingFieldRowPlayer index={5} ship={props.ships[4]} />
                                <PlayingFieldRowPlayer index={6} ship={props.ships[5]} />
                                <PlayingFieldRowPlayer index={7} ship={props.ships[6]} />
                                <PlayingFieldRowPlayer index={8} ship={props.ships[7]} />
                                <PlayingFieldRowPlayer index={9} ship={props.ships[8]} />
                                <PlayingFieldRowPlayer index={10} ship={props.ships[9]} />
                            </Fragment>
                        </div>
                    </div>
                </div>
            </div>
        </Fragment >
    );
}

PlayingFieldPlayer.propTypes = {
    playerName: PropTypes.string,
    ships: PropTypes.array
}

export default PlayingFieldPlayer;