import React from "react";

const GenderSelectionField = ({ handler }) => {
    return (
        <div className="row">
            <label className="col s12">GENDER</label>
            <div className="input-field col s12" id="gender">

                <p>
                    <input id="male" type="radio" name="gender" {...handler()}
                        value="male" />
                    <label htmlFor="male">Male</label>
                </p>

                <p>
                    <input id="female" type="radio" name="gender" {...handler()}
                        value="female" />
                    <label htmlFor="female">Female</label>
                </p>

                <p>
                    <input id="dontknow1" type="radio" name="gender" {...handler()}
                        value="neutral" />
                    <label htmlFor="dontknow1">I prefer not to answer</label>
                </p>
            </div>
        </div>
    );
}

GenderSelectionField.propTypes = {

}

export default GenderSelectionField;