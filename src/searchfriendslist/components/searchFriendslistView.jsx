import React, { Component } from 'react';

import UserListItem from './userListItem';
import {routes} from '../../config';


class SearchFriendsListView extends Component {
    render() {
        return (
            <div className="container">
                <div className="row">
                    <div className="input-field">
                        <input id="search" type="search" required placeholder="Search for a friend..." />
                        <label className="label-icon" htmlFor="search"><i className="material-icons">search</i></label>
                    </div>
                    <div className="col col-s6">
                        <a className="waves-effect waves-light btn">SEARCH</a>
                    </div>
                    <div className="col col-s6">
                        <a className="waves-effect waves-light btn secondary" href={routes.startScreen}>CANCEL</a>
                    </div>
                </div>
                <div className="row">
                    <UserListItem />
                    <UserListItem />
                    <UserListItem />
                    <UserListItem />
                </div>
            </div>
        );
    }
}

export default SearchFriendsListView;