import React from 'react';
import { FieldGroup, FieldControl } from 'react-reactive-form';
import PasswordInput from '../../shared/components/passwordInput';
import PropTypes from 'prop-types';

const ChangePasswordFields = (props) => {
    return (
        <FieldGroup
            control={props.passwordForm}
            render={() => (
                <div className="card">
                    <div className="card-content">
                        <div className="row">
                            <FieldControl
                                name="oldPassword"
                                render={PasswordInput}
                                meta={{ label: "Current password" }}
                            />
                            <FieldControl
                                name="newPassword"
                                render={PasswordInput}
                                meta={{ label: "New password" }}
                            />
                            <FieldControl
                                name="confirmNewPassword"
                                render={PasswordInput}
                                meta={{ label: "Confirm new password" }}
                            />
                        </div>
                    </div>
                </div>
            )}
        />
    );
}

ChangePasswordFields.propTypes ={
    passwordForm: PropTypes.Object
}

export default ChangePasswordFields;